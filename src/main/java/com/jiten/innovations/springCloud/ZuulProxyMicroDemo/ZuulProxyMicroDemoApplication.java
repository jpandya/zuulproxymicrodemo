package com.jiten.innovations.springCloud.ZuulProxyMicroDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.ComponentScan;

@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackageClasses = com.jiten.innovations.springCloud.configuration.ConfigurationClass.class)
public class ZuulProxyMicroDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulProxyMicroDemoApplication.class, args);
	}

}
