package com.jiten.innovations.springCloud.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jiten.innovations.springCloud.filters.AuthHeaderFilter;

@Configuration
public class ConfigurationClass {
	
	@Bean
	AuthHeaderFilter authHeaderFilter() {
	    return new AuthHeaderFilter();
	}
	
}
